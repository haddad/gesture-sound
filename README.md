The VoiceFollower is the first of a series of Max objects, we are currently developing, dedicated to gesture and sound interaction .

TheVoiceFollower  indicates in real-time the time progression of live performance in comparison to a pre-recorded soundfile. This can be used for example to synchronize media processes to the live performance. It was successfully tested for [theater](https://forum.ircam.fr/article/detail/following-the-voice-in-theater/) with spoken or sung voice.

![](https://forum.ircam.fr/media/uploads/Softwares/voicefollower-screenshot1.png) 

## Principle ##

The VoiceFollower requires reading first a reference sound file, that appears on the object window. Markers can be then added to this reference recording.

The synchronization will only work if the text used in the performance is identical with the one used in the reference recording. Thus, this object is not a voice recognition system, but rather a synchronization tool to work with a set of predefined texts.

The message follow is used to start the real-time alignment: the audio signal received by the object is analyzed and appeared as superimposed to the reference recording. The object outputs continuously the time position and the markers labels.
